package com.challenge.movies.domain.interactor

import com.challenge.movies.domain.repository.MoviesRemoteRepository
import com.challenge.movies.model.MoviesResponse
import io.reactivex.Single

class GetTrendingMoviesUseCase(private val moviesRemoteRepository: MoviesRemoteRepository) {

    fun execute(page: Int): Single<MoviesResponse> {
        return moviesRemoteRepository.getTrendingMovies(page)
    }
}