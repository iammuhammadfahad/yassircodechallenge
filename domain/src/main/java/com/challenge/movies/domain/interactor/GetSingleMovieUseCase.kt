package com.challenge.movies.domain.interactor

import com.challenge.movies.domain.repository.MoviesRemoteRepository
import com.challenge.movies.model.MovieDetail
import io.reactivex.Single

class GetSingleMovieUseCase(private val moviesRemoteRepository: MoviesRemoteRepository) {

    fun execute(id: String): Single<MovieDetail> {
        return moviesRemoteRepository.getSingleMovie(id)
    }

}