package com.challenge.movies.domain.repository

import com.challenge.movies.model.MovieDetail
import com.challenge.movies.model.MoviesResponse
import io.reactivex.Single

interface MoviesRemoteRepository {

    fun getTrendingMovies(page: Int): Single<MoviesResponse>

    fun getSingleMovie(id: String): Single<MovieDetail>

}