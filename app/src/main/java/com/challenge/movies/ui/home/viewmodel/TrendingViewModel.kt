package com.challenge.movies.ui.home.viewmodel

import androidx.lifecycle.ViewModel
import com.challenge.movies.data.Resource
import com.challenge.movies.domain.interactor.GetTrendingMoviesUseCase
import com.challenge.movies.model.Movie
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class TrendingViewModel(private val getTrendingMoviesUseCase: GetTrendingMoviesUseCase) :
    ViewModel() {

    private val stateFlow = MutableStateFlow<Resource<List<Movie>>>(Resource.empty())
    private var currentPage = 1
    private var lastPage = 1

    var disposable: Disposable? = null

    val trendingMoviesState: StateFlow<Resource<List<Movie>>>
        get() = stateFlow

    private fun fetchTrendingMovies() {
        stateFlow.value = Resource.loading()

        disposable = getTrendingMoviesUseCase.execute(currentPage)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ res ->
                lastPage = res.total_pages
                stateFlow.value = Resource.success(res.results)
            }, { throwable ->
                lastPage = currentPage
                throwable.localizedMessage?.let {
                    stateFlow.value = Resource.error(it)
                }
            })
    }

    fun fetchNextTrendingMovies() {
        currentPage++
        fetchTrendingMovies()
    }

    fun refreshTrendingMovies() {
        currentPage = 1
        fetchTrendingMovies()
    }

    fun isFirstPage(): Boolean {
        return currentPage == 1
    }

    fun isLastPage(): Boolean {
        return currentPage == lastPage
    }

}