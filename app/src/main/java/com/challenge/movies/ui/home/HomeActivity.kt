package com.challenge.movies.ui.home

import android.os.Bundle
import androidx.fragment.app.add
import androidx.fragment.app.commit
import com.challenge.movies.R
import com.challenge.movies.base.BaseActivity
import com.challenge.movies.databinding.ActivityHomeBinding

class HomeActivity : BaseActivity() {

    private lateinit var binding: ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)

        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                setReorderingAllowed(true)
                add<TrendingFragment>(R.id.nav_host_container)
            }
        }
    }
}