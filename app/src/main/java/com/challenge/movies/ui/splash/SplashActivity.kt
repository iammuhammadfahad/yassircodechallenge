package com.challenge.movies.ui.splash

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import com.challenge.movies.R
import com.challenge.movies.base.BaseActivity
import com.challenge.movies.common.utils.ConnectivityStatus
import com.challenge.movies.ui.home.HomeActivity

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        checkConnectivity()
    }

    private fun checkConnectivity() {
        val connectivity = ConnectivityStatus(this)
        connectivity.observe(this, Observer { isConnected ->
            if (isConnected) {
                startNextActivity()
            } else {
                Toast.makeText(this, R.string.error_message_internet, Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun startNextActivity() {
        Intent(this, HomeActivity::class.java).apply {
            startActivity(this)
        }
    }
}