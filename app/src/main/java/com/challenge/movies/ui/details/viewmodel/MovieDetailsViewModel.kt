package com.challenge.movies.ui.details.viewmodel

import androidx.lifecycle.ViewModel
import com.challenge.movies.data.Resource
import com.challenge.movies.domain.interactor.GetSingleMovieUseCase
import com.challenge.movies.model.MovieDetail
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class MovieDetailsViewModel(private val getSingleMovieUseCase: GetSingleMovieUseCase
) : ViewModel() {

    private val singleMovieStateFlow = MutableStateFlow<Resource<MovieDetail>>(Resource.empty())
    var disposable: Disposable? = null

    val singleMovieState: StateFlow<Resource<MovieDetail>>
        get() = singleMovieStateFlow

    fun fetchSingleMovie(id: String) {
        singleMovieStateFlow.value = Resource.loading()

        disposable = getSingleMovieUseCase.execute(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ res ->
                singleMovieStateFlow.value = Resource.success(res)
            }, { throwable ->
                throwable.localizedMessage?.let {
                    singleMovieStateFlow.value = Resource.error(it)
                }
            })
    }
}