package com.challenge.movies.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.challenge.movies.R
import com.challenge.movies.common.recyclerview.PaginationScrollListener
import com.challenge.movies.common.utils.gone
import com.challenge.movies.common.utils.visible
import com.challenge.movies.data.Resource
import com.challenge.movies.databinding.FragmentMovieListBinding
import com.challenge.movies.model.Movie
import com.challenge.movies.ui.details.MovieDetailsActivity
import com.challenge.movies.ui.home.master.MovieListAdapter
import com.challenge.movies.ui.home.viewmodel.TrendingViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import timber.log.Timber
import java.io.Serializable

class TrendingFragment : Fragment(R.layout.fragment_movie_list),
    MovieListAdapter.OnItemClickListener {

    private val trendingViewModel: TrendingViewModel by sharedViewModel()
    private val movieListAdapter: MovieListAdapter by inject()

    private var _binding: FragmentMovieListBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMovieListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setupRecyclerView()
        setupSwipeRefresh()

        trendingViewModel.refreshTrendingMovies()

        viewLifecycleOwner.lifecycleScope.launch {
            trendingViewModel.trendingMoviesState.collect {
                handleMoviesDataState(it)
            }
        }
    }

    override fun onItemClick(movie: Movie, container: View) {
        val movieExtraData = MovieExtraData()
        movieExtraData.id = movie.id
        movieExtraData.posterPath = movie.poster_path
        val intent = Intent(this.context, MovieDetailsActivity::class.java)
        intent.putExtra("movieData", movieExtraData as Serializable)
        startActivity(intent)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null

        trendingViewModel.disposable?.dispose()
    }

    private fun handleMoviesDataState(state: Resource<List<Movie>>) {
        when (state.status) {
            Resource.Status.LOADING -> {
                binding.srlFragmentMovieList.isRefreshing = true
            }
            Resource.Status.SUCCESS -> {
                binding.srlFragmentMovieList.isRefreshing = false
                loadMovies(state.data)
            }
            Resource.Status.ERROR -> {
                binding.srlFragmentMovieList.isRefreshing = false
                binding.pbFragmentMovieList.gone()
            }
            Resource.Status.EMPTY -> {
                Timber.d("Empty state.")
            }
        }
    }

    private fun loadMovies(movies: List<Movie>?) {
        movies?.let {
            if (trendingViewModel.isFirstPage()) {
                // Remove previous movies
                movieListAdapter.clear()
            }

            movieListAdapter.fillList(it)
        }
    }

    private fun setupRecyclerView() {
        movieListAdapter.setOnMovieClickListener(this)

        binding.rvFragmentMovieList.adapter = movieListAdapter
        binding.rvFragmentMovieList.addOnScrollListener(object :
            PaginationScrollListener(binding.rvFragmentMovieList.linearLayoutManager) {
            override fun isLoading(): Boolean {
                val isLoading = binding.srlFragmentMovieList.isRefreshing

                if (isLoading) {
                    binding.pbFragmentMovieList.visible()
                } else {
                    binding.pbFragmentMovieList.gone()
                }

                return isLoading
            }

            override fun isLastPage(): Boolean {
                return trendingViewModel.isLastPage()
            }

            override fun loadMoreItems() {
                trendingViewModel.fetchNextTrendingMovies()
            }
        })
    }

    private fun setupSwipeRefresh() {
        binding.srlFragmentMovieList.setOnRefreshListener {
            trendingViewModel.refreshTrendingMovies()
        }
    }
}

data class MovieExtraData(
    var id: Int? = null,
    var posterPath: String? = null
) : Serializable