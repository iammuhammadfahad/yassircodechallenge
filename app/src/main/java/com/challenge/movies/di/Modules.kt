package com.challenge.movies.di

import com.challenge.movies.data.repository.MoviesRemoteRepositoryImpl
import com.challenge.movies.data.sources.remote.mapper.MoviesRemoteMapper
import com.challenge.movies.domain.interactor.GetTrendingMoviesUseCase
import com.challenge.movies.domain.interactor.GetSingleMovieUseCase
import com.challenge.movies.domain.repository.MoviesRemoteRepository
import com.challenge.movies.ui.details.viewmodel.MovieDetailsViewModel
import com.challenge.movies.ui.home.master.MovieListAdapter
import com.challenge.movies.ui.home.viewmodel.TrendingViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val mainModule = module {
    single { MoviesRemoteMapper() }
    factory<MoviesRemoteRepository> { MoviesRemoteRepositoryImpl(get()) }
    factory { MovieListAdapter(androidContext()) }
}

val trendingMoviesModule = module {
    factory { GetTrendingMoviesUseCase(get()) }
    viewModel { TrendingViewModel(get()) }
}

val movieDetailsModule = module {
    factory { GetSingleMovieUseCase(get()) }
    viewModel { MovieDetailsViewModel(get()) }
}