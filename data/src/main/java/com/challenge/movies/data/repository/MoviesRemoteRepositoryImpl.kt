package com.challenge.movies.data.repository

import com.challenge.movies.data.sources.remote.api.ApiClient
import com.challenge.movies.data.sources.remote.mapper.MoviesRemoteMapper
import com.challenge.movies.domain.repository.MoviesRemoteRepository
import com.challenge.movies.model.MovieDetail
import com.challenge.movies.model.MoviesResponse
import io.reactivex.Single

class MoviesRemoteRepositoryImpl(private val moviesRemoteMapper: MoviesRemoteMapper) : MoviesRemoteRepository  {

    override fun getTrendingMovies(page: Int): Single<MoviesResponse> {
        return ApiClient.movieService().getTrendingMovies(page).map {
            moviesRemoteMapper.mapFromRemote(it)
        }
    }

    override fun getSingleMovie(id: String): Single<MovieDetail> {
        return ApiClient.movieService().getSingleMovie(id).map {
            moviesRemoteMapper.mapDetailFromRemote(it)
        }
    }

}